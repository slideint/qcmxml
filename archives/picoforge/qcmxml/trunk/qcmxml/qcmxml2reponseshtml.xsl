<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="xml" encoding="utf-8"/>
<xsl:param name="urlform" select="''"/>
<xsl:param name="imgurl" select="''"/>
<!-- suppression des noeuds blancs de type texte inseres automatiquement -->
<xsl:strip-space elements="*"/>

<xsl:template match="/"  xml:space="preserve">
  <xsl:comment><!-- Fichier automatiquement genere --></xsl:comment>
  <br/>
  <h1><b><xsl:value-of select="questionnaire/titre"/> - réponses</b></h1>
  <p>
  <b><i>Instructions pour le QCM :</i></b><br/><xsl:apply-templates select="questionnaire/instructions"/>
  </p>
  <ol>
    <xsl:apply-templates select="questionnaire/question"/>
  </ol>
</xsl:template>

<xsl:template match="question">
  <li><xsl:choose>
      <xsl:when test="@difficulte = 'FACILE'">
	<!--Aucune marque-->
	<xsl:text></xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'INTERMEDIAIRE'">
	<!--Ajout d'une etoile-->
	<xsl:text>*&#xA;</xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'DIFFICILE'">
	<!--Ajout de deux etoiles-->
	<xsl:text>**&#xA;</xsl:text>
      </xsl:when>
    </xsl:choose>
    <!-- copie de intitule en incluant potentiellement les balises HTML -->
    <xsl:apply-templates select="intitule"/>
  </li><xsl:call-template name="sautdeligne"/>
  <table><xsl:call-template name="sautdeligne"/>
    <xsl:apply-templates select="reponse"/>
  </table><xsl:call-template name="sautdeligne"/>
  <br/>
</xsl:template>

<xsl:template match="instructions">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>


<xsl:template match="justification">
<i>(<xsl:apply-templates mode="mixteimage"/>)</i>
</xsl:template>

<xsl:template match="contenu">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>

<xsl:template match="intitule">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>
 
<!-- le mode ici est utilise pour pouvoir afficher du texte, en effet,
     l'affichage de texte a ete desactive par defaut par la derniere
     regle -->

<xsl:template match="text()" mode="mixteimage">
<xsl:value-of select="."/>
</xsl:template> 

<!-- copie en incluant potentiellement les balises HTML -->
<xsl:template match="node()" mode="mixteimage">
  <xsl:copy-of select="."/>
</xsl:template> 

<xsl:template match="img" mode="mixteimage">
    <xsl:choose>
      <xsl:when test="@height and @width">
	<img src="{$imgurl}/{@src}.png" height="{@height}" width="{@width}"/>
      </xsl:when>
      <xsl:when test="@height">
	<img src="{$imgurl}/{@src}.png" height="{@height}"/>
      </xsl:when>
      <xsl:when test="@width">
	<img src="{$imgurl}/{@src}.png" width="{@width}"/>
      </xsl:when>
      <xsl:otherwise>
	<img src="{$imgurl}/{@src}.png"/>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>



<xsl:template match="reponse[@valeur='0']">
  <xsl:param name="qnum"/>
  <tr>
    <td></td><td>faux : <xsl:apply-templates select="contenu"/>  <xsl:apply-templates select="justification"/></td>
  </tr><xsl:call-template name="sautdeligne"/>
</xsl:template>

<xsl:template match="reponse[@valeur]">
  <xsl:param name="qnum"/>
  <tr>
    <td></td><td>vrai : <xsl:apply-templates select="contenu"/> <xsl:apply-templates select="justification"/></td>
  </tr>
</xsl:template>

<xsl:template match="reponse[@statut]">
  <tr>
    <td></td><td><xsl:value-of select="@statut"/> : <xsl:apply-templates select="contenu"/> <xsl:apply-templates select="justification"/></td>
  </tr>
</xsl:template>


<xsl:template name="sautdeligne">
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<!-- par defaut ne rien faire sur les noeuds de type element et texte -->
<xsl:template match="* | text()">
  <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
