<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="xml" encoding="utf-8"/>
<xsl:param name="imgurl" select="''"/>
<!-- suppression des noeuds blancs de type texte inseres automatiquement -->
<xsl:strip-space elements="*"/>

<xsl:template match="/">
  <!-- Fichier automatiquement genere -->
  <quiz> <xsl:text>&#xA;</xsl:text>
    <question type="category"> <xsl:text>&#xA;</xsl:text>
      <category><text>$module$/<xsl:value-of select="questionnaire/idfqcm"/></text></category> <xsl:text>&#xA;</xsl:text>
    </question> <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/><xsl:text>&#xA;</xsl:text>
  </quiz> 
</xsl:template>

<xsl:template match="reponse[@statut='VRAI']">
  <xsl:call-template name="reponsetemplate">
    <xsl:with-param name="valeurfraction" select="100 div count(../reponse[@statut='VRAI'])"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="reponse[@statut='FAUX']">
  <xsl:call-template name="reponsetemplate">
    <xsl:with-param name="valeurfraction" select="-100 div count(../reponse[@statut='VRAI'])"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="reponsetemplate">
  <xsl:param name="valeurfraction"/>

  <xsl:text>&#xA;</xsl:text>
  <answer fraction="{$valeurfraction}"><xsl:text>&#xA;</xsl:text>
  <text> <!-- generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="avantcdata"/>
<xsl:apply-templates select="contenu"/> <!-- end of generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="aprescdata"/></text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="justification"/>
  </answer>
</xsl:template>

<xsl:template match="justification">

  <feedback><text>   
    <xsl:call-template name="avantcdata"/>
    <!-- Affichage en gras -->    
    <span style="font-weight: bold;">
      <xsl:apply-templates select="node()" mode="mixteimage"/>
    </span>
    <xsl:call-template name="aprescdata"/>
    </text></feedback><xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template match="question">
  <xsl:text>&#xA;</xsl:text>
  <question type="multichoice">

    <name><text>
      <!-- generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="avantcdata"/>
      <!-- truncate string to 100 chars for Moodle 2.0 (possibly up to 256 chars) -->
      <xsl:value-of select="substring(intitule, 1, 100)"/>
      <!-- end of generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="aprescdata"/></text></name><xsl:text>&#xA;</xsl:text>
    <questiontext format="moodle-auto-format"><text>
      <!-- generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="avantcdata"/>
    <xsl:choose>
      <xsl:when test="@difficulte = 'FACILE'">
	<!--Aucune marque-->
	<xsl:text></xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'INTERMEDIAIRE'">
	<!--Ajout d'une etoile-->
	<xsl:text>* </xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'DIFFICILE'">
	<!--Ajout de deux etoiles-->
	<xsl:text>** </xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:apply-templates select="intitule"/>
      <!-- end of generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="aprescdata"/></text></questiontext><xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="@difficulte"/>
    <single>false</single>
    <xsl:apply-templates select="reponse"/>
    <xsl:text>&#xA;</xsl:text>
  </question>
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template match="@difficulte">
  <generalfeedback><text>
      <!-- generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="avantcdata"/>
    <!--Affichage en gras-->
      <span style="font-weight: bold;">
	Niveau de difficulté: 
      </span>
    <xsl:choose>
      <xsl:when test=". = 'FACILE'">
	<!--Affichage en gras et vert-->
	<span style="font-weight: bold; color: red;">
	  <xsl:value-of select="."/>
	</span>
      </xsl:when>
      <xsl:when test=". = 'INTERMEDIAIRE'">
	<!--Affichage en gras et orange-->
	<span style="font-weight: bold; color: orange;">
	  <xsl:value-of select="."/>
	</span>
      </xsl:when>
      <xsl:when test=". = 'DIFFICILE'">
	<!--Affichage en gras et rouge-->
	<span style="font-weight: bold; color: green;">
	  <xsl:value-of select="."/>
	</span>
      </xsl:when>
    </xsl:choose>
      <!-- end of generation of CDATA section to protect HTML tags, necessary for moodle -->
      <xsl:call-template name="aprescdata"/>
    </text></generalfeedback><xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template match="contenu">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>
 

<xsl:template match="intitule">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>
 
<!-- copie de intitule avec les balises HTML. Pour ne pas inclure le HTML, il faudrait xsl:select-of select="." -->
<xsl:template match="node()" mode="mixteimage">
<xsl:copy-of select="."/>
</xsl:template> 

<!-- le mode ici est utilise pour pouvoir afficher du texte, en effet,
     l'affichage de texte a ete desactive par defaut par la derniere
     regle -->

<xsl:template match="text()" mode="mixteimage">
<xsl:value-of select="."/>
</xsl:template> 

<xsl:template match="img" mode="mixteimage">
    <xsl:choose>
      <xsl:when test="@height and @width">
	<img src="{$imgurl}/{@src}.png" height="{@height}" width="{@width}"/>
      </xsl:when>
      <xsl:when test="@height">
	<img src="{$imgurl}/{@src}.png" height="{@height}"/>
      </xsl:when>
      <xsl:when test="@width">
	<img src="{$imgurl}/{@src}.png" width="{@width}"/>
      </xsl:when>
      <xsl:otherwise>
	<img src="{$imgurl}/{@src}.png"/>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="sautdeligne">
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template name="avantcdata">
  <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
</xsl:template>

<xsl:template name="aprescdata">
  <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
</xsl:template>

<!-- par defaut ne rien faire sur les noeuds de type element et texte -->
<xsl:template match="* | text()">
  <xsl:apply-templates/>
</xsl:template>


</xsl:stylesheet>
