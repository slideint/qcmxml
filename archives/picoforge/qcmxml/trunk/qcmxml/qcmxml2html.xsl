<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="xml" encoding="utf-8"/>
<!--Ne produit pas du xhtml : <xsl:output method="html" encoding="utf-8" indent="yes"/>-->
<xsl:param name="cgiurl" select="''"/>
<xsl:param name="nombase" select="''"/>
<xsl:param name="imgurl" select="''"/>
<!-- suppression des noeuds blancs de type texte inseres automatiquement -->
<xsl:strip-space elements="*"/>

<xsl:template match="/" xml:space="preserve">
  <xsl:comment><!-- Fichier automatiquement genere --></xsl:comment>
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <title>QCM</title>
      <link rev="made" href="mailto:qcm@int-evry.fr"/>
    </head>
    <body BGCOLOR="#deedfa">
      <br/>
      <center><h1><b><xsl:value-of select="questionnaire/titre"/></b></h1></center>
      <p>
	<table>
	  <tr valign="center" align="left">
	    <td>Auteur(s) :</td><td><b>      
	    <xsl:value-of select="questionnaire/auteurs"/>
	  </b><br/></td></tr>

	  <tr valign="center" align="left"><td>Date : </td><td><b><xsl:value-of select="questionnaire/date"/>
	  </b></td></tr> 
	  <tr valign="center" align="left"><td>Module : </td><td><b><xsl:value-of select="questionnaire/module"/>
	  </b></td></tr>
	  
	  <tr valign="center" align="left">
	    <td><b><i>Instructions pour le QCM :</i></b></td>
	    <td><xsl:apply-templates select="questionnaire/instructions"/></td>
	  </tr>
	</table><xsl:call-template name="sautdeligne"/>
      </p>
	
      <form method="GET" action="{$cgiurl}/traite-qcm-new.cgi">
	<ol>
	  <xsl:apply-templates select="questionnaire/question"/>
	</ol>
	
	<p><input type="checkbox" name="montre_result"/>Afficher les réponses avec le calcul des résultats</p>
	
	<table>
	  <tr>
	    <!--	      <input type="hidden" name="nombase" value="{$nombase}"/> -->
	    <input type="hidden" name="locurl" value="{$nombase}"/>
	    <input type="hidden" name="locrep" value="."/>
	    <td><input type="submit" value="Envoyer les résultats"/></td>
	    <td><input type="reset" value="Tout remettre à zéro"/></td>
	    <td><input type="button" value="Fermer" onClick='top.close();'/></td>
	  </tr>
	</table><xsl:call-template name="sautdeligne"/>
	
      </form>
    </body>
  </html>
</xsl:template>

<xsl:template match="question">
  <li> 
    <xsl:choose>
      <xsl:when test="@difficulte = 'FACILE'">
	<!--Aucune marque-->
	<xsl:text></xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'INTERMEDIAIRE'">
	<!--Ajout d'une etoile-->
	<xsl:text>*&#xA;</xsl:text>
      </xsl:when>
      <xsl:when test="@difficulte = 'DIFFICILE'">
	<!--Ajout de deux etoiles-->
	<xsl:text>**&#xA;</xsl:text>
      </xsl:when>
    </xsl:choose>
<!-- copie de intitule en incluant potentiellement les balises HTML -->
<xsl:apply-templates select="intitule"/>
  </li><xsl:call-template name="sautdeligne"/>
  <table><xsl:call-template name="sautdeligne"/>
    <xsl:apply-templates select="reponse">
      <xsl:with-param name="qnum" select="position()"/>
    </xsl:apply-templates>
  </table><xsl:call-template name="sautdeligne"/>
  <br/>
</xsl:template>

<xsl:template match="instructions">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>

<xsl:template match="contenu">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>
 
<xsl:template match="intitule">
  <xsl:apply-templates mode="mixteimage"/>
</xsl:template>
 
<!-- le mode ici est utilise pour pouvoir afficher du texte, en effet,
     l'affichage de texte a ete desactive par defaut par la derniere
     regle -->

<xsl:template match="text()" mode="mixteimage">
  <xsl:value-of select="."/>
</xsl:template> 

<!-- copie en incluant potentiellement les balises HTML -->
<xsl:template match="node()" mode="mixteimage">
  <xsl:copy-of select="."/>
</xsl:template> 

<xsl:template match="img" mode="mixteimage">
    <xsl:choose>
      <xsl:when test="@height and @width">
	<img src="{$imgurl}/{@src}.png" height="{@height}" width="{@width}"/>
      </xsl:when>
      <xsl:when test="@height">
	<img src="{$imgurl}/{@src}.png" height="{@height}"/>
      </xsl:when>
      <xsl:when test="@width">
	<img src="{$imgurl}/{@src}.png" width="{@width}"/>
      </xsl:when>
      <xsl:otherwise>
	<img src="{$imgurl}/{@src}.png"/>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="reponse">
  <xsl:param name="qnum"/>
  <tr>
    <td valign="top"><input type="checkbox" name="rep_{$qnum}_{position()}"/></td><td><xsl:apply-templates select="contenu"/>
      </td>
    </tr><xsl:call-template name="sautdeligne"/>
</xsl:template>

<xsl:template name="sautdeligne">
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<!-- par defaut ne rien faire sur les noeuds de type element et texte -->
<xsl:template match="* | text()">
  <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
