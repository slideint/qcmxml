<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="text" encoding="utf-8"/>
<xsl:param name="urlform" select="''"/>
<!-- suppression des noeuds blancs de type texte inseres automatiquement -->
<xsl:strip-space elements="*"/>

<xsl:template match="/">
  <xsl:apply-templates select="questionnaire/question"/>
</xsl:template>

<xsl:template match="question">
  <xsl:apply-templates select="reponse" mode="un">
    <xsl:with-param name="qnum" select="position()"/>
  </xsl:apply-templates>
  <xsl:text>q_</xsl:text><xsl:value-of select="position()"/><xsl:text>=</xsl:text><xsl:apply-templates select="reponse" mode="deux"/>
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template match="reponse" mode="un">
  <xsl:param name="qnum"/>
  <xsl:text>rep_</xsl:text><xsl:value-of select="$qnum"/><xsl:text>_</xsl:text><xsl:value-of select="position()"/><xsl:text>=</xsl:text><xsl:apply-templates select="@statut|@valeur" mode="un"/>
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<xsl:template match="@statut[.='VRAI']" mode="un">
  <xsl:text>on</xsl:text>
</xsl:template>

<xsl:template match="@statut[.='FAUX']" mode="un">
  <xsl:text>ko</xsl:text>
</xsl:template>

<xsl:template match="@valeur[.='0']" mode="un">
  <xsl:text>ko</xsl:text>
</xsl:template>

<xsl:template match="@valeur" mode="un">
  <xsl:text>on</xsl:text>
</xsl:template>

<xsl:template match="reponse" mode="deux">
  <xsl:apply-templates select="@statut|@valeur" mode="deux"/>
</xsl:template>

<xsl:template match="@statut[.='VRAI']" mode="deux">
  <xsl:text>:1</xsl:text>
</xsl:template>

<xsl:template match="@statut[.='FAUX']" mode="deux">
  <xsl:text>:2</xsl:text>
</xsl:template>

<xsl:template match="@valeur" mode="deux">
  <xsl:text>:</xsl:text><xsl:value-of select="."/>
</xsl:template>


<xsl:template name="sautdeligne">
  <xsl:text>&#xA;</xsl:text>
</xsl:template>

<!-- par defaut ne rien faire sur les noeuds de type element et texte -->
<xsl:template match="* | text()">
  <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
