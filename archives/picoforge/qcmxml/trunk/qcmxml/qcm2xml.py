#!/usr/bin/python
#

import os,sys

def read_content(file, ligne):
    ligne = file.readline()
    while (ligne[:2] != "--"):
	print ligne.strip()
	ligne = file.readline()
    

if (len(sys.argv) < 2):
    print "ATTENTION"
    print "Usage:", sys.argv[0], "qcmfilename"
    sys.exit()
    
qcmfilename = sys.argv[1]
qcmf = open (qcmfilename, 'r')

basename = qcmfilename[0:-4]

print "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
print "<!DOCTYPE questionnaire SYSTEM \"qcm.dtd\" >"

print "<questionnaire>"
print "<idfqcm>" + basename + "</idfqcm>"
question = 0
ligne = qcmf.readline()

while(ligne):
    ligne = ligne.strip()
    if (ligne ==''):
        print

    elif (ligne == "--titre"):
        print "<titre>"
        read_content(qcmf, ligne)
        print "</titre>"

    elif (ligne == "--date"):
        print "<date>"
        read_content(qcmf, ligne)
        print "</date>"

    elif (ligne == "--auteurs"):
        print "<auteurs>"
        read_content(qcmf, ligne)
        print "</auteurs>"

    elif (ligne == "--instructions"):
        print "<instructions>"
        read_content(qcmf, ligne)
        print "</instructions>"

    elif (ligne == "--module"):
        print "<module>"
        read_content(qcmf, ligne)
        print "</module>"

    elif (ligne == "--localurl"):
        print "<localurl>"
        read_content(qcmf, ligne)
        print "</localurl>"

    elif (ligne == "--localrep"):
        print "<localrep>"
        read_content(qcmf, ligne)
        print "</localrep>"

    elif (ligne == "--question"):
        if (question == 1):
            print "</question>"
            print
            question = 0
        
	question = 1
        ligne = qcmf.readline()
        if (ligne[:2] == "**"):
            print "<question difficulte=\"DIFFICILE\">"
        elif (ligne[:1] == "*"):
            print "<question difficulte=\"INTERMEDIAIRE\">"
        else:
            print "<question difficulte=\"FACILE\">"
	print "<intitule>"
	print ligne.strip()
	read_content(qcmf, ligne)
	print "</intitule>"


    elif ("--reponseok" in ligne):
	print "<reponse statut=\"VRAI\">"
        print "<contenu>"
	read_content(qcmf, ligne)
        print "</contenu>"
	print "</reponse>"

      
    elif ("--reponseko" in ligne):
	print "<reponse statut=\"FAUX\">"
        print "<contenu>"
	read_content(qcmf, ligne)
        print "</contenu>"
	print "</reponse>"
    else:
        if (ligne[0] != '#'):
            sys.stderr.write(sys.argv[0] + " " + qcmfilename + ":" + "cas indetermine " + ligne + " " + str(len(ligne)) + '\n')
        
    ligne = qcmf.readline()

if (question == 1):
    print "</question>"
    print
    question = 0

print "</questionnaire>"
