#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage $0 version repository"
    echo "where  version is in the form yearmonthday"
    echo "and repository is the directory in which bin packages will be stored"
    exit 1
fi
# $1 is the name of the version, in the form yearmonthday
# $2 is the location of the apt repository where to deploy the package
echo '$1' = $1
echo '$2' = $2
# even if the directory debian is not part of the package, export it
rm -rf /tmp/qcmxml-*
cd ../..
svn export trunk /tmp/qcmxml-debian-$1
svn export web/man /tmp/qcmxml-debian-$1/Man
cd /tmp/qcmxml-debian-$1/debian
# rename the directory with the examples
mv ../example_qcmxml ../Examples
# fill in the new version change log
dch -v $1
cp changelog /tmp
cd ..
# -us: non signed source package; -uc: non signed file .changes
dpkg-buildpackage -rfakeroot
#rm -f $2/binary/qcmlxml_*_i386.deb
rm -f $2/binary/qcmxml_*_all.deb
#cp ../qcmxml_$1_i386.deb $2/binary/
cp ../qcmxml_$1_all.deb $2/binary/
rm -f $2/sources/qcmxml_*
cp ../qcmxml_$1.tar.gz ../qcmxml_$1.dsc $2/sources/
cd $2
dpkg-scanpackages binary /dev/null | gzip -9c > binary/Packages.gz
chmod a+r binary/*
dpkg-scansources sources /dev/null | gzip -9c > sources/Sources.gz
chmod a+r sources/*
#rm -rf /tmp/qcmxml*
