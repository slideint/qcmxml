#!/usr/bin/python
#
import sys
import xml.dom.minidom
import latex
import string
import xml.sax
from xml.sax.handler import feature_namespaces

LATEXENCODING = 'latex+latin1'
def print_percent_into_float(size):
    sizelist = size.split("%")
    file.write(str(float(sizelist[0]) / 100.0))


def print_header(file):
#    file.write("\\begin{footnotesize}")
    file.write("\\begin{small}")
    file.write("\\begin{enumerate}")
#    file.write("\\begin{alltt}")


def print_trailer(file):
#    file.write("\end{alltt}")
    file.write("\end{enumerate}")
#    file.write("\end{footnotesize}")
    file.write("\end{small}")

def print_node_image(file, node):
    imgname = node.getAttribute("src");
    imgwidth = node.getAttribute("width");
    imgheight = node.getAttribute("height");
    file.write("\includegraphics")
    if (imgwidth != "" and imgheight != "" ):
        file.write("[width=")
        print_percent_into_float(imgwidth)
        file.write("\\textwidth, height=")
        print_percent_into_float(imgheight)
        file.write("\\textheight]")
    elif (imgwidth != "" ):
        file.write("[width=")
        print_percent_into_float(imgwidth)
        file.write("]")
    elif (imgheight != "" ):
        file.write("[height=")
        print_percent_into_float(imgheight)
        file.write("]")
    file.write("{")
    file.write(imgname)
    file.write("} ")

def print_node_mixteimage(file, node):
    for node2 in node.childNodes:
        if (node2.nodeType == node2.TEXT_NODE):
# suppression de l'appel a .strip() avec le parser SAX
            file.write(node2.nodeValue.encode(LATEXENCODING))
        elif (node2.nodeName == "br"):
            file.write("\\\\ \n")
        elif (node2.nodeName == "code"):
            file.write(" \\texttt{")
            print_node_mixteimage(file, node2)
            file.write("} ")
        elif (node2.nodeName == "img"):
            print_node_image(file, node2)
        elif (node2.nodeName == "pre"):
# on ferme le textit pour inserer un verbatim
            if ((node2.parentNode).nodeName == "justification"):
                file.write(" } ")
            file.write("\\begin{verbatim}")
            print_node_mixteimage(file, node2)
            file.write("\\end{verbatim}")
# on re-ouvre le textit 
            if ((node2.parentNode).nodeName == "justification"):
                file.write(" \\textit{")
        elif (node2.nodeType == node2.ELEMENT_NODE):
            print_node_mixteimage(file, node2)
            
def print_node_instructions(file, node):
#    file.write("\n")
    file.write("\\paragraph{Instructions.}")
    print_node_mixteimage(file, node)
#    file.write("\n")
    
def print_node_intitule(file, node, difficulte):
#    file.write("\n")
    diffcode = ""
    if (difficulte == "INTERMEDIAIRE"):
        diffcode = "*"
    elif (difficulte == "DIFFICILE"):
        diffcode = "**"
#    file.write("\\item \\begin{alltt}")
    file.write("\\item ")
    file.write("%s" % diffcode + " ")
    print_node_mixteimage(file, node)
#    file.write("\end{alltt}\n")
    file.write("\n")


def print_node_reponse(file, node, difficulte):
#    file.write("\\begin{alltt}[")
    file.write("\n\\begin{itemize}\\item[\\begin{tiny}")
    if (node.getAttribute("statut") == "VRAI"):
        file.write("vrai")
    else:
        file.write("faux")
    file.write("\\end{tiny}] ")
        
    for node2 in node.childNodes:
        if (node2.nodeName == "contenu"):
            print_node_mixteimage(file, node2)

        if (node2.nodeName == "justification"):
            file.write("\n \\begin{itemize}\\item[]\\begin{tiny}Justification:\\end{tiny} \\textit{")
            print_node_mixteimage(file, node2)
            file.write("}\\end{itemize} ")
                
#    file.write("\end{alltt}\n")
    file.write("\\end{itemize}\n")


def print_node(file, node, difficulte):
    if (node.nodeName == "intitule"):
        print_node_intitule(file, node, difficulte)
        
    elif (node.nodeName == "reponse"):
        print_node_reponse(file, node, difficulte)

#
# debut du main        
#


if (len(sys.argv) < 2):
    print "ATTENTION"
    print "Usage:", sys.argv[0], "qcmxmlfile"
    sys.exit()

latex.register()

xmlfilename = sys.argv[1]

xmlf = open (xmlfilename, 'r')
# utilisation du parser SAX pour conserver les entites (&nbsp;, &eacute;...)
parser = xml.sax.make_parser()
# le parser SAX est sensible aux namespaces que nous ignorons
# parametrage de SAX pour qu'il ignore les namespaces
parser.setFeature(feature_namespaces, 0)
xmldoc = xml.dom.minidom.parse(xmlf, parser)

file = sys.stdout
if xmldoc.hasChildNodes:
    instructionslist = xmldoc.getElementsByTagName("instructions")
    for instructions in instructionslist:
        print_node_instructions(file, instructions)
        
    print_header(file)

    questions = xmldoc.getElementsByTagName("question")
    for question in questions:
        for node in question.childNodes:
            print_node(file, node, question.getAttribute("difficulte"))
                
    print_trailer(file)

