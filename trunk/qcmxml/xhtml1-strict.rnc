# Extensible HTML version 1.0 Strict DTD
# 
# This is the same as HTML 4 Strict except for
# changes due to the differences between XML and SGML.
# 
# Namespace = http://www.w3.org/1999/xhtml
# 
# For further information, see: http://www.w3.org/TR/xhtml1
# 
# Copyright (c) 1998-2002 W3C (MIT, INRIA, Keio),
# All Rights Reserved. 
# 
# This DTD module is identified by the PUBLIC and SYSTEM identifiers:
# 
# PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
# SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
# 
# $Revision: 1.1 $
# $Date: 2002/08/01 13:56:03 $
#

# ================ Character mnemonic entities =========================

# ================== Imported Names ====================================

default namespace = "http://www.w3.org/1999/xhtml"
namespace a = "http://relaxng.org/ns/compatibility/annotations/1.0"

ContentType = string
# media type, as per [RFC2045]
ContentTypes = string
# comma-separated list of media types, as per [RFC2045]
Charset = string
# a character encoding, as per [RFC2045]
Charsets = string
# a space separated list of character encodings, as per [RFC2045]
LanguageCode = xsd:NMTOKEN
# a language code, as per [RFC3066]
Character = string
# a single character, as per section 2.2 of [XML]
Number = string
# one or more digits
LinkTypes = string
# space-separated list of link types
MediaDesc = string
# single or comma-separated list of media descriptors
URI = string
# a Uniform Resource Identifier, see [RFC2396]
UriList = string
# a space separated list of Uniform Resource Identifiers
Datetime = string
# date and time information. ISO date format
Script = string
# script expression
StyleSheet = string
# style sheet data
Text = string
# used for titles etc.
Length = string
# nn for pixels or nn% for percentage length
MultiLength = string
# pixel, percentage, or relative
Pixels = string
# integer representing length in pixels

# these are used for image maps
Shape = "rect" | "circle" | "poly" | "default"
Coords = string
# comma separated list of lengths

# =================== Generic Attributes ===============================

# core attributes common to most elements
# id       document-wide unique id
# class    space separated list of classes
# style    associated style info
# title    advisory title/amplification
coreattrs =
  attribute id { xsd:ID }?,
  attribute class { text }?,
  attribute style { StyleSheet }?,
  attribute title { Text }?
# internationalization attributes
# lang        language code (backwards compatible)
# xml:lang    language code (as per XML 1.0 spec)
# dir         direction for weak/neutral text
i18n =
  attribute lang { LanguageCode }?,
  attribute xml:lang { LanguageCode }?,
  attribute dir { "ltr" | "rtl" }?
# attributes for common UI events
# onclick     a pointer button was clicked
# ondblclick  a pointer button was double clicked
# onmousedown a pointer button was pressed down
# onmouseup   a pointer button was released
# onmousemove a pointer was moved onto the element
# onmouseout  a pointer was moved away from the element
# onkeypress  a key was pressed and released
# onkeydown   a key was pressed down
# onkeyup     a key was released
events =
  attribute onclick { Script }?,
  attribute ondblclick { Script }?,
  attribute onmousedown { Script }?,
  attribute onmouseup { Script }?,
  attribute onmouseover { Script }?,
  attribute onmousemove { Script }?,
  attribute onmouseout { Script }?,
  attribute onkeypress { Script }?,
  attribute onkeydown { Script }?,
  attribute onkeyup { Script }?
# attributes for elements that can get the focus
# accesskey   accessibility key character
# tabindex    position in tabbing order
# onfocus     the element got the focus
# onblur      the element lost the focus
focus =
  attribute accesskey { Character }?,
  attribute tabindex { Number }?,
  attribute onfocus { Script }?,
  attribute onblur { Script }?
attrs = coreattrs, i18n, events
# =================== Text Elements ====================================
special.pre = br | span | bdo | map
special = special.pre | object | img
fontstyle = tt | i | b | big | small
phrase =
  em
  | strong
  | dfn
  | code
  | q
  | samp
  | kbd
  | var
  | cite
  | abbr
  | acronym
  | sub
  | sup
inline.forms = input | select | textarea | label | button
# these can occur at block or inline level
misc.inline = ins | del | script
# these can only occur at block level
misc = noscript | misc.inline
inline = a | special | fontstyle | phrase | inline.forms
# %Inline; covers inline or "text-level" elements
Inline = (text | inline | misc.inline)*
# ================== Block level elements ==============================
heading = h1 | h2 | h3 | h4 | h5 | h6
lists = ul | ol | dl
blocktext = pre | hr | blockquote | address
block = p | heading | \div | lists | blocktext | fieldset | table
Block = (block | form | misc)*
# %Flow; mixes block and inline and is used for list items etc.
Flow = (text | block | form | inline | misc)*
# ================== Content models for exclusions =====================

# a elements use %Inline; excluding a
a.content =
  (text | special | fontstyle | phrase | inline.forms | misc.inline)*
# pre uses %Inline excluding big, small, sup or sup
pre.content =
  (text
   | a
   | fontstyle
   | phrase
   | special.pre
   | misc.inline
   | inline.forms)*
# form uses %Block; excluding form
form.content = (block | misc)*
# button uses %Flow; but excludes a, form and form controls
button.content =
  (text
   | p
   | heading
   | \div
   | lists
   | blocktext
   | table
   | special
   | fontstyle
   | phrase
   | misc)*
# ================ Document Structure ==================================

# the namespace URI designates the document profile
html = element html { html.attlist, head, body }
html.attlist &=
  i18n,
  attribute id { xsd:ID }?
# ================ Document Head =======================================
head.misc = (script | style | meta | link | object)*
# content model is %head.misc; combined with a single
# title and an optional base element in any order
head =
  element head {
    head.attlist,
    head.misc,
    ((title, head.misc, (base, head.misc)?)
     | (base, head.misc, (title, head.misc)))
  }
head.attlist &=
  i18n,
  attribute id { xsd:ID }?,
  attribute profile { URI }?
# The title element is not considered part of the flow of text.
# It should be displayed, for example as the page header or
# window title. Exactly one title is required per document.
title = element title { title.attlist, text }
title.attlist &=
  i18n,
  attribute id { xsd:ID }?
# document base URI
base = element base { base.attlist, empty }
base.attlist &=
  attribute href { URI },
  attribute id { xsd:ID }?
# generic metainformation
meta = element meta { meta.attlist, empty }
meta.attlist &=
  i18n,
  attribute id { xsd:ID }?,
  attribute http-equiv { text }?,
  attribute name { text }?,
  attribute content { text },
  attribute scheme { text }?
# Relationship values can be used in principle:
# 
#  a) for document specific toolbars/menus when used
#     with the link element in document head e.g.
#       start, contents, previous, next, index, end, help
#  b) to link to a separate style sheet (rel="stylesheet")
#  c) to make a link to a script (rel="script")
#  d) by stylesheets to control how collections of
#     html nodes are rendered into printed documents
#  e) to make a link to a printable version of this document
#     e.g. a PostScript or PDF version (rel="alternate" media="print")
link = element link { link.attlist, empty }
link.attlist &=
  attrs,
  attribute charset { Charset }?,
  attribute href { URI }?,
  attribute hreflang { LanguageCode }?,
  attribute type { ContentType }?,
  attribute rel { LinkTypes }?,
  attribute rev { LinkTypes }?,
  attribute media { MediaDesc }?
# style info, which may include CDATA sections
style = element style { style.attlist, text }
style.attlist &=
  i18n,
  attribute id { xsd:ID }?,
  attribute type { ContentType },
  attribute media { MediaDesc }?,
  attribute title { Text }?,
  [ a:defaultValue = "preserve" ] attribute xml:space { "preserve" }?
# script statements, which may include CDATA sections
script = element script { script.attlist, text }
script.attlist &=
  attribute id { xsd:ID }?,
  attribute charset { Charset }?,
  attribute type { ContentType },
  attribute src { URI }?,
  attribute defer { "defer" }?,
  [ a:defaultValue = "preserve" ] attribute xml:space { "preserve" }?
# alternate content container for non script-based rendering
noscript = element noscript { noscript.attlist, Block }
noscript.attlist &= attrs
# =================== Document Body ====================================
body = element body { body.attlist, Block }
body.attlist &=
  attrs,
  attribute onload { Script }?,
  attribute onunload { Script }?
\div = element div { div.attlist, Flow }
# generic language/style container
div.attlist &= attrs
# =================== Paragraphs =======================================
p = element p { p.attlist, Inline }
p.attlist &= attrs
# =================== Headings =========================================

# There are six levels of headings from h1 (the most important)
# to h6 (the least important).
h1 = element h1 { h1.attlist, Inline }
h1.attlist &= attrs
h2 = element h2 { h2.attlist, Inline }
h2.attlist &= attrs
h3 = element h3 { h3.attlist, Inline }
h3.attlist &= attrs
h4 = element h4 { h4.attlist, Inline }
h4.attlist &= attrs
h5 = element h5 { h5.attlist, Inline }
h5.attlist &= attrs
h6 = element h6 { h6.attlist, Inline }
h6.attlist &= attrs
# =================== Lists ============================================

# Unordered list
ul = element ul { ul.attlist, li+ }
ul.attlist &= attrs
# Ordered (numbered) list
ol = element ol { ol.attlist, li+ }
ol.attlist &= attrs
# list item
li = element li { li.attlist, Flow }
li.attlist &= attrs
# definition lists - dt for term, dd for its definition
dl = element dl { dl.attlist, (dt | dd)+ }
dl.attlist &= attrs
dt = element dt { dt.attlist, Inline }
dt.attlist &= attrs
dd = element dd { dd.attlist, Flow }
dd.attlist &= attrs
# =================== Address ==========================================

# information on author
address = element address { address.attlist, Inline }
address.attlist &= attrs
# =================== Horizontal Rule ==================================
hr = element hr { hr.attlist, empty }
hr.attlist &= attrs
# =================== Preformatted Text ================================

# content is %Inline; excluding "img|object|big|small|sub|sup"
pre = element pre { pre.attlist, pre.content }
pre.attlist &=
  attrs,
  [ a:defaultValue = "preserve" ] attribute xml:space { "preserve" }?
# =================== Block-like Quotes ================================
blockquote = element blockquote { blockquote.attlist, Block }
blockquote.attlist &=
  attrs,
  attribute cite { URI }?
# =================== Inserted/Deleted Text ============================

# ins/del are allowed in block and inline content, but its
# inappropriate to include block content within an ins element
# occurring in inline content.
ins = element ins { ins.attlist, Flow }
ins.attlist &=
  attrs,
  attribute cite { URI }?,
  attribute datetime { Datetime }?
del = element del { del.attlist, Flow }
del.attlist &=
  attrs,
  attribute cite { URI }?,
  attribute datetime { Datetime }?
# ================== The Anchor Element ================================

# content is %Inline; except that anchors shouldn't be nested
a = element a { a.attlist, a.content }
a.attlist &=
  attrs,
  focus,
  attribute charset { Charset }?,
  attribute type { ContentType }?,
  attribute name { xsd:NMTOKEN }?,
  attribute href { URI }?,
  attribute hreflang { LanguageCode }?,
  attribute rel { LinkTypes }?,
  attribute rev { LinkTypes }?,
  [ a:defaultValue = "rect" ] attribute shape { Shape }?,
  attribute coords { Coords }?
# ===================== Inline Elements ================================
span = element span { span.attlist, Inline }
# generic language/style container
span.attlist &= attrs
bdo = element bdo { bdo.attlist, Inline }
# I18N BiDi over-ride
bdo.attlist &=
  coreattrs,
  events,
  attribute lang { LanguageCode }?,
  attribute xml:lang { LanguageCode }?,
  attribute dir { "ltr" | "rtl" }
br = element br { br.attlist, empty }
# forced line break
br.attlist &= coreattrs
em = element em { em.attlist, Inline }
# emphasis
em.attlist &= attrs
strong = element strong { strong.attlist, Inline }
# strong emphasis
strong.attlist &= attrs
dfn = element dfn { dfn.attlist, Inline }
# definitional
dfn.attlist &= attrs
code = element code { code.attlist, Inline }
# program code
code.attlist &= attrs
samp = element samp { samp.attlist, Inline }
# sample
samp.attlist &= attrs
kbd = element kbd { kbd.attlist, Inline }
# something user would type
kbd.attlist &= attrs
var = element var { var.attlist, Inline }
# variable
var.attlist &= attrs
cite = element cite { cite.attlist, Inline }
# citation
cite.attlist &= attrs
abbr = element abbr { abbr.attlist, Inline }
# abbreviation
abbr.attlist &= attrs
acronym = element acronym { acronym.attlist, Inline }
# acronym
acronym.attlist &= attrs
q = element q { q.attlist, Inline }
# inlined quote
q.attlist &=
  attrs,
  attribute cite { URI }?
sub = element sub { sub.attlist, Inline }
# subscript
sub.attlist &= attrs
sup = element sup { sup.attlist, Inline }
# superscript
sup.attlist &= attrs
tt = element tt { tt.attlist, Inline }
# fixed pitch font
tt.attlist &= attrs
i = element i { i.attlist, Inline }
# italic font
i.attlist &= attrs
b = element b { b.attlist, Inline }
# bold font
b.attlist &= attrs
big = element big { big.attlist, Inline }
# bigger font
big.attlist &= attrs
small = element small { small.attlist, Inline }
# smaller font
small.attlist &= attrs
# ==================== Object ======================================

# object is used to embed objects as part of HTML pages.
# param elements should precede other content. Parameters
# can also be expressed as attribute/value pairs on the
# object element itself when brevity is desired.
object =
  element object {
    object.attlist, (text | param | block | form | inline | misc)*
  }
object.attlist &=
  attrs,
  attribute declare { "declare" }?,
  attribute classid { URI }?,
  attribute codebase { URI }?,
  attribute data { URI }?,
  attribute type { ContentType }?,
  attribute codetype { ContentType }?,
  attribute archive { UriList }?,
  attribute standby { Text }?,
  attribute height { Length }?,
  attribute width { Length }?,
  attribute usemap { URI }?,
  attribute name { xsd:NMTOKEN }?,
  attribute tabindex { Number }?
# param is used to supply a named property value.
# In XML it would seem natural to follow RDF and support an
# abbreviated syntax where the param elements are replaced
# by attribute value pairs on the object start tag.
param = element param { param.attlist, empty }
param.attlist &=
  attribute id { xsd:ID }?,
  attribute name { text }?,
  attribute value { text }?,
  [ a:defaultValue = "data" ]
  attribute valuetype { "data" | "ref" | "object" }?,
  attribute type { ContentType }?
# =================== Images ===========================================

# To avoid accessibility problems for people who aren't
# able to see the image, you should provide a text
# description using the alt and longdesc attributes.
# In addition, avoid the use of server-side image maps.
# Note that in this DTD there is no name attribute. That
# is only available in the transitional and frameset DTD.
img = element img { img.attlist, empty }
img.attlist &=
  attrs,
  attribute src { URI },
  attribute alt { Text },
  attribute longdesc { URI }?,
  attribute height { Length }?,
  attribute width { Length }?,
  attribute usemap { URI }?,
  attribute ismap { "ismap" }?
# usemap points to a map element which may be in this document
# or an external document, although the latter is not widely supported

# ================== Client-side image maps ============================

# These can be placed in the same document or grouped in a
# separate document although this isn't yet widely supported
map =
  element map {
    map.attlist,
    ((block | form | misc)+ | area+)
  }
map.attlist &=
  i18n,
  events,
  attribute id { xsd:ID },
  attribute class { text }?,
  attribute style { StyleSheet }?,
  attribute title { Text }?,
  attribute name { xsd:NMTOKEN }?
area = element area { area.attlist, empty }
area.attlist &=
  attrs,
  focus,
  [ a:defaultValue = "rect" ] attribute shape { Shape }?,
  attribute coords { Coords }?,
  attribute href { URI }?,
  attribute nohref { "nohref" }?,
  attribute alt { Text }
# ================ Forms ===============================================
form = element form { form.attlist, form.content }
# forms shouldn't be nested
form.attlist &=
  attrs,
  attribute action { URI },
  [ a:defaultValue = "get" ] attribute method { "get" | "post" }?,
  [ a:defaultValue = "application/x-www-form-urlencoded" ]
  attribute enctype { ContentType }?,
  attribute onsubmit { Script }?,
  attribute onreset { Script }?,
  attribute accept { ContentTypes }?,
  attribute accept-charset { Charsets }?
# Each label must not contain more than ONE field
# Label elements shouldn't be nested.
label = element label { label.attlist, Inline }
label.attlist &=
  attrs,
  attribute for { xsd:IDREF }?,
  attribute accesskey { Character }?,
  attribute onfocus { Script }?,
  attribute onblur { Script }?
InputType =
  "text"
  | "password"
  | "checkbox"
  | "radio"
  | "submit"
  | "reset"
  | "file"
  | "hidden"
  | "image"
  | "button"
# the name attribute is required for all but submit & reset
input = element input { input.attlist, empty }
# form control
input.attlist &=
  attrs,
  focus,
  [ a:defaultValue = "text" ] attribute type { InputType }?,
  attribute name { text }?,
  attribute value { text }?,
  attribute checked { "checked" }?,
  attribute disabled { "disabled" }?,
  attribute readonly { "readonly" }?,
  attribute size { text }?,
  attribute maxlength { Number }?,
  attribute src { URI }?,
  attribute alt { text }?,
  attribute usemap { URI }?,
  attribute onselect { Script }?,
  attribute onchange { Script }?,
  attribute accept { ContentTypes }?
select = element select { select.attlist, (optgroup | option)+ }
# option selector
select.attlist &=
  attrs,
  attribute name { text }?,
  attribute size { Number }?,
  attribute multiple { "multiple" }?,
  attribute disabled { "disabled" }?,
  attribute tabindex { Number }?,
  attribute onfocus { Script }?,
  attribute onblur { Script }?,
  attribute onchange { Script }?
optgroup = element optgroup { optgroup.attlist, option+ }
# option group
optgroup.attlist &=
  attrs,
  attribute disabled { "disabled" }?,
  attribute label { Text }
option = element option { option.attlist, text }
# selectable choice
option.attlist &=
  attrs,
  attribute selected { "selected" }?,
  attribute disabled { "disabled" }?,
  attribute label { Text }?,
  attribute value { text }?
textarea = element textarea { textarea.attlist, text }
# multi-line text field
textarea.attlist &=
  attrs,
  focus,
  attribute name { text }?,
  attribute rows { Number },
  attribute cols { Number },
  attribute disabled { "disabled" }?,
  attribute readonly { "readonly" }?,
  attribute onselect { Script }?,
  attribute onchange { Script }?
# The fieldset element is used to group form fields.
# Only one legend element should occur in the content
# and if present should only be preceded by whitespace.
fieldset =
  element fieldset {
    fieldset.attlist, (text | legend | block | form | inline | misc)*
  }
fieldset.attlist &= attrs
legend = element legend { legend.attlist, Inline }
# fieldset label
legend.attlist &=
  attrs,
  attribute accesskey { Character }?
# Content is %Flow; excluding a, form and form controls
button = element button { button.attlist, button.content }
# push button
button.attlist &=
  attrs,
  focus,
  attribute name { text }?,
  attribute value { text }?,
  [ a:defaultValue = "submit" ]
  attribute type { "button" | "submit" | "reset" }?,
  attribute disabled { "disabled" }?
# ======================= Tables =======================================

# Derived from IETF HTML table standard, see [RFC1942]

# The border attribute sets the thickness of the frame around the
# table. The default units are screen pixels.
# 
# The frame attribute specifies which parts of the frame around
# the table should be rendered. The values are not the same as
# CALS to avoid a name clash with the valign attribute.
TFrame =
  "void"
  | "above"
  | "below"
  | "hsides"
  | "lhs"
  | "rhs"
  | "vsides"
  | "box"
  | "border"
# The rules attribute defines which rules to draw between cells:
# 
# If rules is absent then assume:
#     "none" if border is absent or border="0" otherwise "all"
TRules = "none" | "groups" | "rows" | "cols" | "all"
# horizontal alignment attributes for cell contents
# 
# char        alignment char, e.g. char=':'
# charoff     offset for alignment char
cellhalign =
  attribute align { "left" | "center" | "right" | "justify" | "char" }?,
  attribute char { Character }?,
  attribute charoff { Length }?
# vertical alignment attributes for cell contents
cellvalign =
  attribute valign { "top" | "middle" | "bottom" | "baseline" }?
table =
  element table {
    table.attlist,
    caption?,
    (col* | colgroup*),
    thead?,
    tfoot?,
    (tbody+ | tr+)
  }
caption = element caption { caption.attlist, Inline }
thead = element thead { thead.attlist, tr+ }
tfoot = element tfoot { tfoot.attlist, tr+ }
tbody = element tbody { tbody.attlist, tr+ }
colgroup = element colgroup { colgroup.attlist, col* }
col = element col { col.attlist, empty }
tr = element tr { tr.attlist, (th | td)+ }
th = element th { th.attlist, Flow }
td = element td { td.attlist, Flow }
table.attlist &=
  attrs,
  attribute summary { Text }?,
  attribute width { Length }?,
  attribute border { Pixels }?,
  attribute frame { TFrame }?,
  attribute rules { TRules }?,
  attribute cellspacing { Length }?,
  attribute cellpadding { Length }?
caption.attlist &= attrs
# colgroup groups a set of col elements. It allows you to group
# several semantically related columns together.
colgroup.attlist &=
  attrs,
  [ a:defaultValue = "1" ] attribute span { Number }?,
  attribute width { MultiLength }?,
  cellhalign,
  cellvalign
# col elements define the alignment properties for cells in
# one or more columns.
# 
# The width attribute specifies the width of the columns, e.g.
# 
#     width=64        width in screen pixels
#     width=0.5*      relative width of 0.5
# 
# The span attribute causes the attributes of one
# col element to apply to more than one column.
col.attlist &=
  attrs,
  [ a:defaultValue = "1" ] attribute span { Number }?,
  attribute width { MultiLength }?,
  cellhalign,
  cellvalign
# Use thead to duplicate headers when breaking table
# across page boundaries, or for static headers when
# tbody sections are rendered in scrolling panel.
# 
# Use tfoot to duplicate footers when breaking table
# across page boundaries, or for static footers when
# tbody sections are rendered in scrolling panel.
# 
# Use multiple tbody sections when rules are needed
# between groups of table rows.
thead.attlist &= attrs, cellhalign, cellvalign
tfoot.attlist &= attrs, cellhalign, cellvalign
tbody.attlist &= attrs, cellhalign, cellvalign
tr.attlist &= attrs, cellhalign, cellvalign
# Scope is simpler than headers attribute for common tables
Scope = "row" | "col" | "rowgroup" | "colgroup"
# th is for headers, td for data and for cells acting as both
th.attlist &=
  attrs,
  attribute abbr { Text }?,
  attribute axis { text }?,
  attribute headers { xsd:IDREFS }?,
  attribute scope { Scope }?,
  [ a:defaultValue = "1" ] attribute rowspan { Number }?,
  [ a:defaultValue = "1" ] attribute colspan { Number }?,
  cellhalign,
  cellvalign
td.attlist &=
  attrs,
  attribute abbr { Text }?,
  attribute axis { text }?,
  attribute headers { xsd:IDREFS }?,
  attribute scope { Scope }?,
  [ a:defaultValue = "1" ] attribute rowspan { Number }?,
  [ a:defaultValue = "1" ] attribute colspan { Number }?,
  cellhalign,
  cellvalign
