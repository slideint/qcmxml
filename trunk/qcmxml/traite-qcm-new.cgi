#! /usr/bin/perl 
# CGI qui traite un QCM et affiche le r�sultat
# voir perldoc CGI
use File::Basename;
use CGI;

my $requete = new CGI;
my @parametres = $requete->param;

# on recupere le nom de base du QCM qu'on traite
# ceci permet d'avoir un CGI unique pour plusieurs QCMs
my $nombase = basename($requete->param('locurl'), ".html");
my $nomurl = $requete->param('locurl');
my $locrep = $requete->param('locrep');
my $fichierreponses = "$locrep/$nombase-reponses.txt";
my $reponseshtml = "$locrep/$nombase-reponses.html";

open REPONSES, $fichierreponses or die("Ouverture de $fichierreponses impossible, $!\n");
my $reponse = new CGI(REPONSES);
my @reponses = $reponse->param;

print $requete->header(-charset=>'utf-8');

# d�but du HTML
print $requete->start_html(-title => 'Resultats du QCM',
			   -author => 'qcm@int-evry.fr', -BGCOLOR=>'#deedfa');

print "\n<h1>R&eacute;sultat du QCM</h1><p>";

$score = 0;
$erreurgrave = 0;
$nbquestion = 0;
$nbbonl=-1;
$nbfauxl=0;
$bdgravel=0;

my $chaine_binaire = ":";
for $i ( @reponses ) {
	my $parametre = $requete->param($i);
	if ($i !~ /^q_/) {
	# on construit la chaine binaire correspondant aux reponses faites par l'eleve
	# cette chaine est une serie de 1 et de 0 separe par des : (pour pouvoir la decomposer par un split)
		$i =~ /_(\d+)_/;
		my $qcount = $1;
		
		if (! $parametre ) {
			$requete->param($i, "off");
		}

		my $chaine_binaire = $requete->param("q_$qcount") . ":" .
			($parametre eq "on" ? "1" : "0");
		$requete->param("q_$qcount", $chaine_binaire);
	} else {
	# on fait la comparaison entre la chaine binaire des reponses eleves et la chaine binaire du corrige
		$i =~ /q_(\d+)/;
		my $qcount = $1;
		$chaine_binaire = ":";
		$nbbonl=-1;
		$nbfauxl=0;
		$nbgravel=0;
		$nbquestion++;
		# $l1=$requete->param($i);
		# $l2=$reponse->param($i);
		# print "l1=$l1 l2=$l2<p>\n";
		@rep=split/:/,$requete->param($i);
		@req=split/:/,$reponse->param($i);
		$j=0;
		while ($j <= $#req) {
		# print "$req[$j] : $rep[$j]<br>\n";
			if ($req[$j] eq $rep[$j] || ($req[$j] eq '2' && $rep[$j] eq '0')) {
			# egalite entre la reponse et le corrige ou bien non reponse sur une erreur grave
				$nbbonl+=1;
				}
			else {
			# inegalite entre reponse et corrige donc erreur
				$nbfauxl+=1;
				if ($req[$j] eq '2' && $rep[$j] eq '1') {
				# en plus il s'agit d'une erreur grave
					$nbgravel+=1;
					}
				}
			$j+=1;
		}
		if ($nbfauxl eq '0') {
			$score +=1;
			print "<p>R&eacute;ponse $qcount bonne</p>\n";
			}
		else {
			$erreurgrave += $nbgravel;
			print "<p>R&eacute;ponse $qcount fausse<ul>\n";
			print "<li>$nbbonl bon(s) item(s)<br><li>$nbfauxl mauvais item(s) dont $nbgravel erreur(s) grave(s)</ul></p>\n";
			}
					
	}
}

print "<hr><strong><h2>Bilan</h2><p>Votre score est de $score sur $nbquestion.<p>Vous avez $erreurgrave erreur(s) grave(s)</strong><p>\n";
print "<hr>\n";

# on inclut ou non les reponses
if ($requete->param('montre_result') eq "on") {
	open REPONSESHTML, "$reponseshtml"
		or die("Ouverture du fichier de r�ponses impossible, $!\n");

	while (<REPONSESHTML>) {
		print;
	}
	print "<hr>\n";
	close(REPONSESHTML);
}

print "<p><a href=\"$nomurl\">Recommencer le QCM</a>&nbsp;&nbsp;&nbsp;&nbsp;<form><input type=button value=Fermer onClick='top.close();'>";

# fin du HTML
print $requete->end_html;
print "\n";
close(REPONSES);
